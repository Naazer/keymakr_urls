## Url Shortener App

### Install
* Install Symfony 4 from git repository
* Configure MySQL, nginx/Apache, php and set variables
* Copy .env to .env.local
* Set environments to .env.local
* run 
```
composer install
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```
or
```
./provision.sh
```
### Environments (in .env)
* **HASHIDS_SALT** - SALT for [HashIds](https://hashids.org/)
* **HASHIDS_MIN_LENGTH** - Minimum length for generated cipher for [HashIds](https://hashids.org/)
* **MAX_OBJECTS** - Maximum unique URLs
* **MAX_URL_SUBMISSIONS** - Maximum submissions of URLs per **PER_TIME**
* **MAX_URL_ACCESSES** - Maximum count of URLs accesses per **PER_TIME**
* **PER_TIME** - Count of seconds

### How to scale application
If you want to scale application up to 10,000 URL submissions and 10,000,000
short URL accesses per second with up to 10,000,000,000 unique URLs you have to change in .env.local:
```
MAX_OBJECTS=10000000000
MAX_URL_SUBMISSIONS=10000
MAX_URL_ACCESSES=10000000
PER_TIME=1
```

