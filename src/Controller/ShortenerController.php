<?php

namespace App\Controller;

use App\Exception\UrlNotFountException;
use App\Service\ShortenerGetter;
use App\Service\ShortenerSetter;
use App\Exception\NotValidUrlException;
use App\Factory\ShortenerUrlEntityFactory;
use App\Exception\MaxUrlAccessesException;
use App\Exception\MaxUrlSubmissionsException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ShortenerController extends AbstractController
{
    /**
     * @param Request $request
     * @param ShortenerSetter $shortener
     * @param ShortenerUrlEntityFactory $factory
     * @return RedirectResponse|Response
     * @throws NotValidUrlException
     *
     * @Route(
     *     "/",
     *     methods={"GET","POST","HEAD"},
     *     name="shortener_encode"
     * )
     */
    public function encodeAction(
        Request $request,
        ShortenerSetter $shortener,
        ShortenerUrlEntityFactory $factory
    ) {
        $form = $this->createFormBuilder([])
            ->add('url', UrlType::class, ['label' => 'url:'])
            ->add('save', SubmitType::class, ['label' => 'generate short url'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $shortenerUrlObj = $shortener->generate($factory->createFromArray($data));
            } catch (NotValidUrlException|MaxUrlSubmissionsException $e) {
                $this->addFlash('warning', $e->getMessage());
            }
            if(!empty($shortenerUrlObj)) {
                $this->addFlash('url', $shortenerUrlObj->getCipher());
            }
        }

        $current = $shortener->currentObjects();
        $max = $shortener->maxObjects();

        return $this->render('shortener/index.html.twig', [
            'form' => $form->createView(),
            'current' => $current,
            'max' => $max,
            'percent' => ceil(($current / $max) * 100)
        ]);
    }

    /**
     * @Route(
     *     "/{cipher}",
     *     name="shortener_redirect",
     *     methods={"GET"},
     *     requirements={
     *          "cipher"="\S+"
     *     }
     * )
     */
    public function redirectAction($cipher, ShortenerGetter $shortener)
    {
        try {
            $url = $shortener->get($cipher);

            return $url;
        } catch (MaxUrlAccessesException|UrlNotFountException $e) {
            $this->addFlash('warning', $e->getMessage());

            return $this->render('shortener/redirect.html.twig');
        }
    }
}