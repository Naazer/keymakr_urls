<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShortenerUrlRepository")
 * @ORM\Table(name="shortener_url", indexes={@ORM\Index(name="idx", columns={"id", "url"})})
 * @ORM\HasLifecycleCallbacks
 */
class ShortenerUrl
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true, options={"collation":"utf8_bin"})
     * @JMS\Expose()
     */
    private $cipher;

    /**
     * @Assert\NotBlank(message="The url should not be blank")
     * @Assert\Url
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $url;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=true, options={"default" : 0})
     * @JMS\Expose()
     */
    private $beats;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @JMS\Type("DateTime<'Y-m-d H:i:s', 'UTC'>")
     * @JMS\Expose()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @JMS\Type("DateTime<'Y-m-d H:i:s', 'UTC'>")
     * @JMS\Expose()
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShortenerUrlAccess", mappedBy="url")
     */
    private $shortenerUrlAccesses;

    public function __construct()
    {
        $this->shortenerUrlAccesses = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getCipher(): ?string
    {
        return $this->cipher;
    }

    /**
     * @inheritDoc
     */
    public function setCipher(?string $cipher): self
    {
        $this->cipher = $cipher;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @inheritDoc
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBeats(): ?int
    {
        return $this->beats;
    }

    /**
     * @inheritDoc
     */
    public function setBeats(int $beats): self
    {
        $this->beats = $beats;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addBeats($increment = 1): self
    {
        $this->beats += $increment;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|ShortenerUrlAccess[]
     */
    public function getShortenerUrlAccesses(): Collection
    {
        return $this->shortenerUrlAccesses;
    }

    public function addShortenerUrlAccess(ShortenerUrlAccess $shortenerUrlAccess): self
    {
        if (!$this->shortenerUrlAccesses->contains($shortenerUrlAccess)) {
            $this->shortenerUrlAccesses[] = $shortenerUrlAccess;
            $shortenerUrlAccess->setUrl($this);
        }

        return $this;
    }

    public function removeShortenerUrlAccess(ShortenerUrlAccess $shortenerUrlAccess): self
    {
        if ($this->shortenerUrlAccesses->contains($shortenerUrlAccess)) {
            $this->shortenerUrlAccesses->removeElement($shortenerUrlAccess);
            // set the owning side to null (unless already changed)
            if ($shortenerUrlAccess->getUrl() === $this) {
                $shortenerUrlAccess->setUrl(null);
            }
        }

        return $this;
    }
}
