<?php


namespace App\Exception;

use Exception;

/**
 * Class UrlNotFountException
 * @package App\Exception
 */
class UrlNotFountException extends Exception
{

}