<?php


namespace App\Exception;

use Exception;

/**
 * Class NotValidUrlException
 * @package App\Exception
 */
class NotValidUrlException extends Exception
{

}