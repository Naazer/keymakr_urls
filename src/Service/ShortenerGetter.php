<?php


namespace App\Service;

use App\Entity\ShortenerUrlAccess;
use App\Exception\MaxUrlAccessesException;
use App\Exception\UrlNotFountException;
use App\Repository\ShortenerUrlRepository;
use App\Repository\ShortenerUrlAccessRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ShortenerGetter
{
    /**
     * @var ShortenerUrlRepository
     */
    private $repoUrl;

    /**
     * @var ShortenerUrlAccessRepository
     */
    private $repoAccess;

    /**
     * @var int
     */
    private $perTime;

    /**
     * @var int
     */
    private $maxUrlAccesses;

    /**
     * ShortenerGetter constructor.
     * @param ShortenerUrlRepository $repoUrl
     * @param ShortenerUrlAccessRepository $repoAccess
     * @param int $maxUrlAccesses
     * @param int $perTime
     */
    public function __construct(
        ShortenerUrlRepository $repoUrl,
        ShortenerUrlAccessRepository $repoAccess,
        int $maxUrlAccesses,
        int $perTime
    ) {
        $this->repoUrl = $repoUrl;
        $this->repoAccess = $repoAccess;
        $this->maxUrlAccesses = $maxUrlAccesses;
        $this->perTime = $perTime;
    }

    /**
     * @param string $cipher
     * @return RedirectResponse|null
     * @throws MaxUrlAccessesException
     * @throws UrlNotFountException
     */
    public function get($cipher)
    {
        $shortenerUrlObj = $this->repoUrl->findByCipher($cipher);
        if (!$shortenerUrlObj) {
            throw new UrlNotFountException('Route not found');
        }
        $countPerSeconds = $this->repoAccess->countPerSeconds($this->perTime);
        if ($countPerSeconds < $this->maxUrlAccesses) {
            $shortenerUrlObj->addBeats();
            $shortenerUrlObj = $this->repoUrl->save($shortenerUrlObj);
            $shortenerUrlAccessObj = new ShortenerUrlAccess();
            $shortenerUrlObj->addShortenerUrlAccess($shortenerUrlAccessObj);
            $this->repoAccess->save($shortenerUrlAccessObj);
            $url = $shortenerUrlObj->getUrl();

            return new RedirectResponse($url);
        } else {
            throw new MaxUrlAccessesException("You have exceeded the maximum number of urls accesses.");
        }
    }
}