<?php


namespace App\Service;

use App\Exception\MaxUrlSubmissionsException;
use Hashids\Hashids;
use App\Entity\ShortenerUrl;
use App\Repository\ShortenerUrlRepository;

class ShortenerSetter
{
    /**
     * @var ShortenerUrlRepository
     */
    private $repo;

    /**
     * @var Hashids
     */
    private $hashids;

    /**
     * @var int
     */
    private $maxObjects;

    /**
     * @var int
     */
    private $maxUrlSubmissions;

    /**
     * @var int
     */
    private $perTime;

    /**
     * ShortenerSetter constructor.
     * @param ShortenerUrlRepository $repo
     * @param Hashids $hashids
     * @param int $maxObjects
     * @param int $maxUrlSubmissions
     * @param int $perTime
     */
    public function __construct(
        ShortenerUrlRepository $repo,
        Hashids $hashids,
        int $maxObjects,
        int $maxUrlSubmissions,
        int $perTime
    ) {
        $this->repo = $repo;
        $this->hashids = $hashids;
        $this->maxObjects = $maxObjects;
        $this->maxUrlSubmissions = $maxUrlSubmissions;
        $this->perTime = $perTime;
    }

    /**
     * @param ShortenerUrl $shortenerUrl
     * @return ShortenerUrl|null
     * @throws MaxUrlSubmissionsException
     */
    public function generate(ShortenerUrl $shortenerUrl)
    {
        $countPerSeconds = $this->repo->countPerSeconds($this->perTime);
        if ($countPerSeconds < $this->maxUrlSubmissions) {
            if ($this->repo->countTotalUrls() < $this->maxObjects) {
                return $this->generateNew($shortenerUrl);
            } else {
                return $this->updatePrevious($shortenerUrl);
            }
        } else {
            throw new MaxUrlSubmissionsException("You have exceeded the maximum number of urls submissions.");
        }
    }

    /**
     * @return mixed
     */
    public function currentObjects()
    {
        return $this->repo->countTotalUrls();
    }

    /**
     * @return int
     */
    public function maxObjects()
    {
        return $this->maxObjects;
    }

    /**
     * @param $shortenerUrl
     * @return ShortenerUrl|null
     */
    private function generateNew($shortenerUrl)
    {
        $existingShortUrl = $this->repo->findMatchingByUrl($shortenerUrl);
        if (!is_null($existingShortUrl)) {
            return $existingShortUrl;
        }
        $this->repo->save($shortenerUrl);
        $id = $shortenerUrl->getId();
        $cipher = $this->hashids->encode($id);
        $shortenerUrl->setCipher($cipher);

        return $this->repo->save($shortenerUrl);
    }

    /**
     * @param $shortenerUrl
     * @return ShortenerUrl
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function updatePrevious($shortenerUrl)
    {
        $earlierObj = $this->repo->findEarlierObject();
        $earlierObj->setCreatedAt(new \DateTime("now"));
        $earlierObj->setUrl($shortenerUrl->getUrl());
        $earlierObj->setBeats(0);

        return $this->repo->save($earlierObj);
    }
}