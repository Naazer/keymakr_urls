<?php


namespace App\Factory;

use App\Entity\ShortenerUrl;
use App\Exception\NotValidUrlException;

/**
 * Class ShortenerUrlEntityFactory
 * @package App\Factory
 */
class ShortenerUrlEntityFactory
{
    /**
     * @var string
     */
    protected $entity;

    /**
     * ShortenerUrlEntityFactory constructor.
     * @param string $entity
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param array $data
     * @return ShortenerUrl
     * @throws NotValidUrlException
     */
    public function createFromArray(array $data): ShortenerUrl
    {
        // validate url
        if (!isset($data['url']) || (filter_var($data['url'], FILTER_VALIDATE_URL) === false)) {
            throw new NotValidUrlException('your url is not valid');
        }
        $url = rtrim($data['url'], '/');
        /** @var ShortenerUrl $obj */
        $obj = new $this->entity();
        $obj->setUrl($url);

        return $obj;
    }
}