<?php

namespace App\Repository;

use App\Entity\ShortenerUrl;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ShortenerUrl|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShortenerUrl|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShortenerUrl[]    findAll()
 * @method ShortenerUrl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShortenerUrlRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShortenerUrl::class);
    }

    public function findMatchingByCipher(ShortenerUrl $shortenerUrl)
    {
        $qb = $this->createQueryBuilder('su');
        $qb->where('su.cipher = :cipher')
            ->setParameter('cipher', $shortenerUrl->getCipher());

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findMatchingByUrl(ShortenerUrl $shortenerUrl)
    {
        $qb = $this->createQueryBuilder('su');
        $qb->where('su.url = :url')
            ->andWhere('su.cipher IS NOT NULL')
            ->setParameter('url', $shortenerUrl->getUrl());
        $results = $qb->getQuery()->getResult();

        return (0 === count($results)) ? null : $results[0];
    }

    public function findByCipher($cipher)
    {
        $qb = $this->createQueryBuilder('su');
        $qb->where('su.cipher = :cipher')
            ->setParameter('cipher', $cipher);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function countTotalUrls()
    {
        return $this->createQueryBuilder('su')
            ->select('COUNT(1)')
            ->andWhere('su.cipher IS NOT NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countPerSeconds($seconds)
    {
        $date = new \DateTime();
        $date->modify(sprintf('-%s seconds', $seconds));

        return $this->createQueryBuilder('su')
            ->select('COUNT(1)')
            ->andWhere('su.createdAt > :date')
            ->andWhere('su.cipher IS NOT NULL')
            ->setParameter(':date', $date)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return ShortenerUrl|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findEarlierObject()
    {
        return $this->createQueryBuilder('su')
            ->setMaxResults(1)
            ->andWhere('su.cipher IS NOT NULL')
            ->orderBy('su.createdAt', 'asc')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(ShortenerUrl $shortenerUrl)
    {
        $this->_em->persist($shortenerUrl);
        $this->_em->flush($shortenerUrl);

        return $shortenerUrl;
    }
}
