<?php

namespace App\Repository;

use App\Entity\ShortenerUrlAccess;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ShortenerUrlAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShortenerUrlAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShortenerUrlAccess[]    findAll()
 * @method ShortenerUrlAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShortenerUrlAccessRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShortenerUrlAccess::class);
    }

    public function countPerSeconds($seconds)
    {
        $date = new \DateTime();
        $date->modify(sprintf('-%s seconds', $seconds));

        return $this->createQueryBuilder('sua')
            ->select('COUNT(1)')
            ->andWhere('sua.createdAt > :date')
            ->setParameter(':date', $date)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function save(ShortenerUrlAccess $shortenerUrlAccess)
    {
        $this->_em->persist($shortenerUrlAccess);
        $this->_em->flush($shortenerUrlAccess);

        return $shortenerUrlAccess;
    }
}
